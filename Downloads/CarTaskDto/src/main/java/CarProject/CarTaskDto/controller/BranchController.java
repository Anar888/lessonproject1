package CarProject.CarTaskDto.controller;

import CarProject.CarTaskDto.dto.BranchRequest;
import CarProject.CarTaskDto.dto.BranchResponse;
import CarProject.CarTaskDto.dto.MarketResponse;
import CarProject.CarTaskDto.service.BranchService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/market")
@RequiredArgsConstructor
public class BranchController {

    private final BranchService branchService;

    @PostMapping("/{marketId}")
    public BranchResponse create(@PathVariable Long marketId, @RequestBody BranchRequest request) {
        return branchService.create(marketId, request);
    }

    @PutMapping("/{marketId}/branch/{branchId}")
    public BranchResponse update(@PathVariable Long marketId,
                                 @PathVariable Long branchId,
                                 @RequestBody BranchRequest request) {
        return branchService.update(marketId, branchId, request);
    }

    @GetMapping("/branches")
    public List<BranchResponse> getAll(){
        return branchService.getAll();
    }
    @GetMapping("/branches/{branchId}")
    public BranchResponse get(@PathVariable Long branchId) {
        return branchService.get(branchId);
    }
    @DeleteMapping("/branches/{branchId}")
    public void delete( @PathVariable Long branchId ) {
         branchService.delete( branchId);
    }

}

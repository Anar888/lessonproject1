package ProjectLesson1.az.service;

import ProjectLesson1.az.dto.TaskRequest;
import ProjectLesson1.az.dto.TaskResponse;
import ProjectLesson1.az.model.AppUser;
import ProjectLesson1.az.model.Task;
import ProjectLesson1.az.repository.AppUserRepository;
import ProjectLesson1.az.repository.OrganizationRepository;
import ProjectLesson1.az.repository.TaskRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class TaskService {
    private  final TaskRepository taskRepository;
    private  final AppUserRepository appUserRepository;
    private  final OrganizationRepository organizationRepository;
    private  final ModelMapper modelMapper;
    public TaskResponse createTaskToUser(Long taskId, Long userId,Long organizatinId) {
        Task task = taskRepository.findById(taskId)
                .orElseThrow(() -> new RuntimeException(String.format("Task with id %s not found", taskId)));

        AppUser user=appUserRepository.findById(userId)
                .orElseThrow(() -> new RuntimeException(String.format("Task with id %s not found", userId)));

        if (task.getOrganization().equals(organizationRepository.getOrgById(organizatinId))){
            task.getUsers().add(user);
        }
        else{
            new RuntimeException();
        }

        taskRepository.save(task);
        return modelMapper.map(task, TaskResponse.class);
    }


    public TaskResponse create(TaskRequest request) {
        Task task = modelMapper.map(request, Task.class);
        task = taskRepository.save(task);
        return modelMapper.map(task, TaskResponse.class);
    }
    @Transactional
    public TaskResponse get(Long taskid) {
        Task task = taskRepository.findById(taskid)
                .orElseThrow(() -> new RuntimeException(String
                        .format("Task with id %s not found", taskid)));
        return modelMapper.map(task,TaskResponse.class);
    }
    public void delete(Long taskid) {
        taskRepository.deleteById(taskid);
    }
    public void update(Long taskid, TaskRequest request) {
        Task task = taskRepository.findById(taskid)
                .orElseThrow(() -> new RuntimeException(String
                        .format("Task with id %s not found", taskid)));
        task.setName(request.getName());
       task.setDeadline(request.getDeadline());
       task.setDescription(request.getDescription());
       task.setStatus(request.getStatus());
       taskRepository.save(task);

    }
}

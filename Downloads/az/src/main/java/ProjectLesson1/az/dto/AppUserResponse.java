package ProjectLesson1.az.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AppUserResponse {

    Long id;

    String name;
    String surname;
    String email;
    String password;
}

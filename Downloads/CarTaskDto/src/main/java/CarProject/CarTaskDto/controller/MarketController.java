package CarProject.CarTaskDto.controller;

import CarProject.CarTaskDto.dto.MarketRequest;
import CarProject.CarTaskDto.dto.MarketResponse;
import CarProject.CarTaskDto.service.MarketService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/market")
@RequiredArgsConstructor
public class MarketController {

    private final MarketService marketService;

    @PostMapping
    public MarketResponse create(@RequestBody @Valid MarketRequest request) {
        return marketService.create(request);
    }

    @GetMapping("/{marketId}")
    public MarketResponse get(@PathVariable Long marketId) {
        return marketService.get(marketId);
    }
    @GetMapping
    public List<MarketResponse> getAll() {
        return marketService.getAll();
    }

    @DeleteMapping("/{marketId}")
    public void delete(@PathVariable Long marketId) {
        marketService.delete(marketId);
    }
    @PutMapping("/{marketId}")
    public void update(@PathVariable Long marketId,@RequestBody MarketRequest request ){
        marketService.update(marketId,request);
    }
}


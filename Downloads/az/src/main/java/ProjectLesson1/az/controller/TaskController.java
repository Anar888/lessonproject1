package ProjectLesson1.az.controller;

import ProjectLesson1.az.dto.AppUserRequest;
import ProjectLesson1.az.dto.AppUserResponse;
import ProjectLesson1.az.dto.TaskRequest;
import ProjectLesson1.az.dto.TaskResponse;
import ProjectLesson1.az.service.AppUserService;
import ProjectLesson1.az.service.TaskService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/task")
@RequiredArgsConstructor
public class TaskController {
    private  final TaskService taskService;
    @PostMapping("/{taskId}/{userId}")
    public TaskResponse createTaskToUser(@PathVariable Long taskId,@PathVariable Long userId,@PathVariable Long organizatinId){
        return taskService.createTaskToUser(taskId,userId,organizatinId);
    }

    @PostMapping
    public TaskResponse create(@RequestBody  TaskRequest request) {
        return taskService.create(request);
    }

    @GetMapping("/{taskid}")
    public TaskResponse get(@PathVariable Long taskid) {
        return taskService.get(taskid);
    }


    @DeleteMapping("/{taskid}")
    public void delete(@PathVariable Long taskid) {
        taskService.delete(taskid);
    }
    @PutMapping("/{taskid}")
    public void update(@PathVariable Long taskid,@RequestBody TaskRequest request ){
        taskService.update(taskid,request);
    }

}

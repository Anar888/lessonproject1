package ProjectLesson1.az.model;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "organization")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Organization {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String name;
    @OneToMany(mappedBy = "organization", cascade = CascadeType.ALL)
    @Builder.Default
    List<Task> tasks = new ArrayList<>();
}

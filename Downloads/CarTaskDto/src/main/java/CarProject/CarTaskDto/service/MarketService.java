package CarProject.CarTaskDto.service;

import CarProject.CarTaskDto.dto.MarketRequest;
import CarProject.CarTaskDto.dto.MarketResponse;
import CarProject.CarTaskDto.model.Market;
import CarProject.CarTaskDto.repository.MarketRepository;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MarketService {

    private final MarketRepository marketRepository;
    private final ModelMapper modelMapper;

    public MarketResponse create(MarketRequest request) {
        Market market = modelMapper.map(request, Market.class);
        market = marketRepository.save(market);
        return modelMapper.map(market, MarketResponse.class);
    }

    @Transactional
    public MarketResponse get(Long marketId) {
        Market market = marketRepository.findById(marketId)
                .orElseThrow(() -> new RuntimeException(String.format("Market with id %s not found", marketId)));
        return modelMapper.map(market,MarketResponse.class);
    }

    public void delete(Long marketId) {
        marketRepository.deleteById(marketId);
    }

    public void update(Long marketId, MarketRequest request) {
        Market market = marketRepository.findById(marketId)
                .orElseThrow(() -> new RuntimeException(String.format("Market with id %s not found", marketId)));
        market.setName(request.getName());
        market.setType(request.getType());
        marketRepository.save(market);

    }

    public List<MarketResponse> getAll() {
        return marketRepository
                .findAll()
                .stream()
                .map(market -> modelMapper.map(market, MarketResponse.class))
                .collect(Collectors.toList());

    }
}


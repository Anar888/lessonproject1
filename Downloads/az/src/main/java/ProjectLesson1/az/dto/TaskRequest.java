package ProjectLesson1.az.dto;

import ProjectLesson1.az.enums.StatusType;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDateTime;
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TaskRequest {

    String name;
    LocalDateTime deadline;
    String description;
    StatusType status;
}

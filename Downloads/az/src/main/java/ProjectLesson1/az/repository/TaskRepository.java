package ProjectLesson1.az.repository;

import ProjectLesson1.az.model.AppUser;
import ProjectLesson1.az.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

public interface TaskRepository extends JpaRepository<Task,Long> {


    @Query("select t from Task t join fetch t.users u where u.id = :id")
    Collection<Task> findAllByAppUserId(Long id);


}

package ProjectLesson1.az;

import ProjectLesson1.az.model.AppUser;
import ProjectLesson1.az.model.Organization;
import ProjectLesson1.az.model.Task;
import ProjectLesson1.az.repository.AppUserRepository;
import ProjectLesson1.az.repository.OrganizationRepository;
import ProjectLesson1.az.repository.TaskRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDateTime;
import java.time.Month;

import static ProjectLesson1.az.enums.StatusType.ACTIVE;

@SpringBootApplication
@RequiredArgsConstructor
public class AzApplication implements CommandLineRunner {
private final OrganizationRepository organizationRepository;
private final TaskRepository taskRepository;
private  final AppUserRepository appUserRepository;

	public static void main(String[] args) {
		SpringApplication.run(AzApplication.class, args);
	}
	@Override
	@Transactional
	public void run(String... args) throws Exception {
//		LocalDateTime localDateTime =
//				LocalDateTime.of(2023, Month.APRIL, 28, 14, 33, 48, 000000);
//		Task task=Task
//				.builder().name("Task2").description("desc2").deadline(localDateTime).status(ACTIVE).build();
//		AppUser user1=AppUser.builder().name("user1").
//				email("mjsjdj@mail.ru").surname("surname1").
//				password("1234").build();
//
//		task.getUsers().add(user1);
//appUserRepository.save(user1);
//taskRepository.save(task);

//		System.out.println(taskRepository.findAllByAppUserId(1l));
	}
}

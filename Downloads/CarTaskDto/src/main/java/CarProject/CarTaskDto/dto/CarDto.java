package CarProject.CarTaskDto.dto;

import lombok.Data;

@Data
public class CarDto {
    private Integer id;
    private String color;
    private Integer engine;
    private String model;
    private String maker;
}

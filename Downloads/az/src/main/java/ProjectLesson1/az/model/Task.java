package ProjectLesson1.az.model;

import ProjectLesson1.az.enums.StatusType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jdk.jshell.Snippet;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tasks")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String name;
    LocalDateTime deadline;
    String description;
    StatusType status;
    @JsonIgnore
    @ManyToOne
    @ToString.Exclude
    Organization organization;
    @ManyToMany(cascade = CascadeType.PERSIST)
    @Builder.Default
    @JoinTable(name = "task_appuser",
            joinColumns = @JoinColumn(name = "task_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "appuser_id", referencedColumnName = "id"))
    List<AppUser> users = new ArrayList<>();
}

package CarProject.CarTaskDto.repository;

import CarProject.CarTaskDto.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address,Long> {
}
